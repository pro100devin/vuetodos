package features;

import org.testng.annotations.Test;
import utils.runner.ApiTestRunner;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;

public class DummyTests extends ApiTestRunner {

    @Test
    public void verifyEmployeeName() {
        given().pathParam("id", "2")
                .when().get("/employee/{id}")
                .then().statusCode(200)
                .and().body("data.employee_name", equalTo("Garrett Winters"));
    }

    @Test
    public void verifyEmployeesCount() {
        given()
                .when().get("/employees")
                .then().statusCode(200)
                .and().body("data.employee_name.size()", equalTo(4));
    }
}
