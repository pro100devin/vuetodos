package features;

import org.testng.annotations.Test;
import steps.BaseSteps;
import utils.runner.UiTestRunner;

import java.util.List;

import static java.util.List.of;
import static utils.StringUtil.getRandomString;

public class VueJsTests extends UiTestRunner {
    private final BaseSteps baseSteps = new BaseSteps();
    private List<String> itemsList = of(getRandomString(), getRandomString(), getRandomString());

    @Test
    public void verifyTitle() {
        baseSteps
                .openVueTodos()
                .verifyTitle();
    }

    @Test
    public void verifyAddingItems() {
        baseSteps
                .openVueTodos()
                .addItems(itemsList)
                .verifyIsItemsAdded(itemsList);
    }

    @Test
    public void verifyUpdatingItems() {
        baseSteps
                .openVueTodos()
                .addItems(itemsList)
                .updateItem(itemsList.get(1), getRandomString())
                .verifyIsItemsAdded(itemsList);
    }

    @Test
    public void verifyCompleteItems() {
        var item = itemsList.get(1);
        baseSteps
                .openVueTodos()
                .addItems(itemsList)
                .completeItem(item)
                .verifyIsItemComplete(item);
    }
}
