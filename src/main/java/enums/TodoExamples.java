package enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum TodoExamples {
    VUE("Vue", "Vue.js"),
    ANGULAR_JS("Angular", "Angular.js");

    private final String shortName;
    private final String fullName;
}
