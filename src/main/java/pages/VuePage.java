package pages;

import org.openqa.selenium.Keys;

import java.util.List;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Selenide.*;
import static java.lang.String.format;

public class VuePage {
    private String ITEM_NAME_LOCATOR = "//label[text()='%s']";

    public String getHeaderName() {
        return $("header>h3").getText();
    }

    public void addItem(String itemName) {
        $x("//input[@placeholder='What needs to be done?']")
                .val(itemName)
                .pressEnter();
    }

    public void updateItem(String oldItemName, String newItemName) {
        var editableItem = $x(format(ITEM_NAME_LOCATOR, oldItemName))
                .doubleClick()
                .parent()
                .$x("./following-sibling::input");

        editableItem.sendKeys(Keys.CONTROL + "a");
        editableItem.sendKeys(newItemName);

        $x(format(ITEM_NAME_LOCATOR, newItemName))
                .parent()
                .$x("./following-sibling::input")
                .pressEnter();
    }

    public List<String> getItemsList() {
        return $$x("//ul[@class='todo-list']//label").texts();
    }

    public void completeItem(String itemName) {
        $x(format(ITEM_NAME_LOCATOR, itemName)).parent().$x("./input").click();
    }

    public boolean isItemComplete(String itemName) {
        return $x(format(ITEM_NAME_LOCATOR, itemName))
                .parent()
                .parent()
                .getAttribute("class")
                .contains("completed");
    }
}
