package steps;

import io.qameta.allure.Step;
import pages.VuePage;

import java.util.List;

import static enums.TodoExamples.VUE;
import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;

public class VueSteps {
    private final VuePage vuePage = new VuePage();

    @Step("Vue todos: added item")
    public VueSteps addItems(List<String> itemsList) {
        itemsList.forEach(vuePage::addItem);

        return this;
    }

    public void verifyTitle() {
        assertThat(vuePage.getHeaderName())
                .as(format("Header should be %s", VUE.getFullName()))
                .isEqualTo(VUE.getFullName());
    }

    public void verifyIsItemsAdded(List<String> itemsList) {
        assertThat(vuePage.getItemsList())
                .as("Items list should be updated")
                .containsExactlyInAnyOrderElementsOf(itemsList);
    }

    @Step("Vue todos: updated item {oldItemName} to {newItemName}")
    public VueSteps updateItem(String oldItemName, String newItemName) {
        vuePage.updateItem(oldItemName, newItemName);

        return this;
    }

    @Step("Vue todos: completed item {itemName}")
    public VueSteps completeItem(String itemName) {
        vuePage.completeItem(itemName);

        return this;
    }

    public void verifyIsItemComplete(String itemName) {
        assertThat(vuePage.isItemComplete(itemName))
                .as("Item should be completed")
                .isTrue();
    }
}
