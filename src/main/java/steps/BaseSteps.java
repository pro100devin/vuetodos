package steps;

import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.open;
import static java.lang.String.format;
import static utils.AllureUtil.log;
import static utils.PropertyHelper.getTodoHost;

public class BaseSteps {

    @Step("Vue todos: opened todos")
    public VueSteps openVueTodos() {
        var mainPage = format("%svue", getTodoHost());
        log(mainPage);
        open(mainPage);

        return new VueSteps();
    }
}
