package utils;

import lombok.experimental.UtilityClass;

import static org.apache.commons.lang3.RandomStringUtils.random;

@UtilityClass
public class StringUtil {

    public static String getRandomString() {
        return random(10, true, false);
    }
}
