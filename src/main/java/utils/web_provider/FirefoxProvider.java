package utils.web_provider;

import com.codeborne.selenide.WebDriverProvider;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import static io.github.bonigarcia.wdm.WebDriverManager.firefoxdriver;

public class FirefoxProvider extends AbstractProvider implements WebDriverProvider {

    @Override
    public WebDriver createDriver(DesiredCapabilities desiredCapabilities) {
        firefoxdriver().setup();

        return new FirefoxDriver(getFirefoxOptions());
    }
}
