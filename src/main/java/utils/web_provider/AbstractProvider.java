package utils.web_provider;

import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;

import java.net.MalformedURLException;
import java.net.URL;

import static java.util.List.of;
import static org.openqa.selenium.PageLoadStrategy.NORMAL;

public abstract class AbstractProvider {

    public URL selenoidUrl;

    {
        try {
            selenoidUrl = new URL("http://localhost:4444/wd/hub");
        } catch (MalformedURLException e) {
            throw new AssertionError("Malformed url: " + e.getMessage());
        }

    }

    public ChromeOptions getChromeOptions() {
        var CHROME_OPTIONS = of(
                "no-sandbox",
//                "headless",
                "disable-site-isolation-trials",
                "ignore-certificate-errors",
                "disable-popup-blocking",
                "disable-notifications",
                "disable-browser-side-navigation",
                "disable-gpu",
                "dns-prefetch-disable",
                "disable-impl-side-painting",
                "disable-infobars",
                "disable-dev-shm-usage"
        );

        var chromeOptions = new ChromeOptions();
        chromeOptions
                .addArguments(CHROME_OPTIONS)
                .setPageLoadStrategy(NORMAL);

        return chromeOptions;
    }

    public FirefoxOptions getFirefoxOptions(){
        FirefoxBinary firefoxBinary = new FirefoxBinary();
        FirefoxOptions options = new FirefoxOptions();
        FirefoxProfile profile = new FirefoxProfile();

        //Accept Untrusted Certificates
        profile.setAcceptUntrustedCertificates(true);
        profile.setAssumeUntrustedCertificateIssuer(false);
        //Use No Proxy Settings
        profile.setPreference("network.proxy.type", 0);
        //Set Firefox profile to capabilities
        options.setCapability(FirefoxDriver.PROFILE, profile);
        options.setBinary(firefoxBinary);
        options.setHeadless(true);

        return options;
    }


}
