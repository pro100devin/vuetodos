package utils.web_provider;

import com.codeborne.selenide.WebDriverProvider;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class RemoteChromeProvider extends AbstractProvider implements WebDriverProvider {

    @Override
    public WebDriver createDriver(DesiredCapabilities desiredCapabilities) {
        var chromeOptions = getChromeOptions();

        return new RemoteWebDriver(selenoidUrl, chromeOptions);
    }
}
