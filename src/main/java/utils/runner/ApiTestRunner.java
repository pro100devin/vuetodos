package utils.runner;

import io.restassured.RestAssured;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

public class ApiTestRunner {
    @BeforeClass(alwaysRun = true)
    protected void setUpBrowser() {
        RestAssured.baseURI ="http://dummy.restapiexample.com/api/v1/";
    }
}
