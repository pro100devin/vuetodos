package utils.runner;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import utils.web_provider.ChromeProvider;
import utils.web_provider.FirefoxProvider;
import utils.web_provider.RemoteChromeProvider;
import utils.web_provider.RemoteFirefoxProvider;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static java.lang.System.getProperty;
import static java.time.Duration.ofSeconds;
import static java.util.concurrent.TimeUnit.SECONDS;

public class UiTestRunner {

    @BeforeMethod(alwaysRun = true)
    @Parameters(value = {"browser"})
    protected void setUpBrowser(String browser) {
        Configuration.browserSize = "1920x1080";
        Configuration.screenshots = false;
        Configuration.savePageSource = false;
        Configuration.baseUrl = "https://todomvc.com/";
        Configuration.pageLoadTimeout = ofSeconds(300).toMillis();

        var isRemote = getProperty("is.remote", "false").equalsIgnoreCase("true");
        if (browser.equals("chrome")) {
            Configuration.browser = isRemote
                    ? RemoteChromeProvider.class.getName()
                    : ChromeProvider.class.getName();
        } else {
            Configuration.browser = isRemote
                    ? RemoteFirefoxProvider.class.getName()
                    : FirefoxProvider.class.getName();
        }

        open("about:blank");
        getWebDriver()
                .manage()
                .timeouts()
                .setScriptTimeout(30, SECONDS);
    }

    @AfterMethod(alwaysRun = true)
    protected void closeWebDriver() {
        WebDriverRunner.closeWebDriver();
    }


}