package utils;

import lombok.experimental.UtilityClass;

import java.util.ResourceBundle;

import static java.util.ResourceBundle.getBundle;

@UtilityClass
public class PropertyHelper {
    private static final ResourceBundle bundle = getBundle("global");

    public static String getTodoHost() {
        return bundle.getString("todomvc.host");
    }

    public static String getFakeApiHost() {
        return bundle.getString("fake.api.host");
    }

}
