#!groovy

pipeline {
    agent {
        node {
            label 'test_env'
        }
    }

    parameters {
        string(name: 'BRANCH', defaultValue: 'master', description: '')
        string(name: 'TESTS', defaultValue: 'features.VueJsTests#verifyTitle', description: 'what need to run: features.ClassName#methodName')
        choice(name: 'BROWSER', choices: ['chrome', 'firefox'], description: '')
    }

    stages {
        stage('Whoami') {
            steps {
                script {
                    sh "whoami"
                }
            }
        }

        stage('Pull changes from repo') {
            steps {
                script {
                    checkout([$class                           : 'GitSCM',
                              branches                         : [[name: '${BRANCH}']],
                              doGenerateSubmoduleConfigurations: false,
                              extensions                       : [],
                              submoduleCfg                     : [],
                              userRemoteConfigs                : [[credentialsId: 'BitBucket', url: 'https://pro100devin@bitbucket.org/pro100devin/vuetodos.git']]])
                }
            }
        }

//        stage('Install Java 11') {
//            steps {
//                script {
//                    sh "sudo apt-get update"
//                    sh "sudo apt-get -qqy install openjdk-11-jdk"
//                    sh "java -version"
//                }
//            }
//        }

//        stage('Install Firefox v96') {
//            steps {
//                script {
//                    sh "sudo apt install firefox -y"
//                }
//            }
//        }

//        stage('Install Chrome v96') {
//            steps {
//                script {
//                    sh "curl -sSLo /home/jenkins/chrome.deb https://github.com/webnicer/chrome-downloads/raw/master/x64.deb/google-chrome-stable_96.0.4664.110-1_amd64.deb"
//                    sh "sudo dpkg -i /home/jenkins/chrome.deb"
//                    sh "google-chrome --version"
//                }
//            }
//        }

//        stage('Install ChromeDriver v96') {
//            steps {
//                script {
//                    sh "chromedriver --version"
//                    sh "rm -rf /usr/local/bin/chromedriver"
//                    sh "curl --silent --show-error --location --fail --retry 3 --output /tmp/chromedriver_linux64.zip \"http://chromedriver.storage.googleapis.com/96.0.4664.18/chromedriver_linux64.zip\""
//                    sh "unzip /tmp/chromedriver_linux64.zip && rm -rf /tmp/chromedriver_linux64.zip"
//                    sh "mv /tmp/chromedriver /usr/local/bin/chromedriver"
//                    sh "chmod +x /usr/local/bin/chromedriver"
//                    sh "chromedriver --version"
//                }
//            }
//        }

        stage('Run tests') {
            steps {
                catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
                    script {
                        sh "export DBUS_SESSION_BUS_ADDRESS=/dev/null"
                        //-q - Quiet output - only show errors
                        sh "mvn -q test -am -Dtest=${params.TESTS} -Dbrowser=${params.BROWSER}"
                    }
                }
            }
        }

        stage('Generate Allure report') {
            steps {
                script {
                    zip archive: true, dir: 'allure-results', glob: '', zipFile: 'allure-results.zip'
                    allure([
                            includeProperties: false,
                            jdk              : '',
                            properties       : [],
                            reportBuildPolicy: 'ALWAYS',
                            results          : [[path: 'allure-results']]
                    ])
                }
            }
        }
    }

//    post {
//        always {
//            archiveArtifacts artifacts: '**/build/reports/tests/*.png', fingerprint: true
//        }
//    }
}
